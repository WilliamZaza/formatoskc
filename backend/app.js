'use strict'

var express = require('express');
var bodyParser = require('body-parser');

var app= express();


//middlewares
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json);

//rutas
app.get('/test', (req,res) =>{
res.status(200).send("<h1>Hola SKC</h1>"
);
});

//exportar
module.exports = app;